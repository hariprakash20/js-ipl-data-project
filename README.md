# JS IPL Data project
In this project i have practiced to extract raw data from the files matches.csv and deliveries.csv, which contains the records of the IPL match which was conducted from 2008 to 2017. The task was to extract data from the csv files, convert it to json format and store it in a json file according to the 9 different problem statements mentioned below.
 * Number of matches played per year for all the years in IPL.
 * Number of matches won per team per year in IPL.
 * Extra runs conceded per team in the year 2016
 * Top 10 economical bowlers in the year 2015
 * Find the number of times each team won the toss and also won the match
 * Find a player who has won the highest number of Player of the Match awards for each season
 * Find the strike rate of a batsman for each season
 * Find the highest number of times one player has been dismissed by another player
 * Find the bowler with the best economy in super overs

The logic and conversion of the file happens in .cjs files and the output is written in the .json file. The files in the repository are organized in the below fashion.

---
- src/
  - server/
    - 1-matches-per-year.cjs
    - 2-matches-won-per-team-per-year.cjs
    - ...
  - public/
    - output
        - 1-matches-per-year.json
        - 2-matches-won-per-team-per-year.json
        - ...
  - data/
    - matches.csv
    - deliveries.csv
- package.json
- package-lock.json
- .gitignore 
---