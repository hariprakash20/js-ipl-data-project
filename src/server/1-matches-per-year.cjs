const csvFilePath='src/data/matches.csv'
const csvtojson=require('csvtojson');
const fs = require('fs');
let years = [];
csvtojson()
.fromFile(csvFilePath)
.then((jsonObj)=>{
   for(let i=0;i<jsonObj.length;i++){
    years.push(jsonObj[i].season);
   }
   const matchesPerYear = {};
   years.forEach((year) => { matchesPerYear[year] = (matchesPerYear[year] || 0) + 1; });
   console.log(matchesPerYear);
   fs.writeFileSync("src/public/output/1-matches-per-year.json", JSON.stringify(matchesPerYear));
});