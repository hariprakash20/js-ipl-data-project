const csvDeliveriesFilePath='src/data/deliveries.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObj)=>{
    let bowlers = [];
    for(let i in jsonObj){
        if(jsonObj[i].is_super_over != '0'){
            let found = false;
            for(j in bowlers){
                if(jsonObj[i].bowler == bowlers[j].name)
                {
                    let runsConceded = parseInt(jsonObj[i].total_runs) - parseInt(jsonObj[i].bye_runs) - parseInt(jsonObj[i].legbye_runs) - parseInt(jsonObj[i].penalty_runs);
                    bowlers[j].runsConceded += runsConceded;
                    if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
                        bowlers[j].ballsBowled++;
                    }
                    found = true;
                }
            }
            if(!found){
                let runsConceded = parseInt(jsonObj[i].total_runs) - parseInt(jsonObj[i].bye_runs) - parseInt(jsonObj[i].legbye_runs) - parseInt(jsonObj[i].penalty_runs);
                let ballsBowled  = 0;
                if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
                    ballsBowled = 1;
                }
                bowlers.push(new bowler(jsonObj[i].bowler, runsConceded, ballsBowled));
            }  
        }
    }
    calculateEconomy(bowlers);
    bowlers.sort(compare);
    console.log(bowlers);
    fs.writeFileSync('src/public/output/9-best-bowler-economy-in-superovers.json',JSON.stringify(bowlers[0])); 
})

function bowler(name, runsConceded, ballsBowled){
    this.name = name;
    this.runsConceded = runsConceded;
    this.ballsBowled = ballsBowled;
    this.economy = 0;
}

function calculateEconomy(bowlers){
    for(i in bowlers){
        bowlers[i].economy = Number((parseFloat(bowlers[i].runsConceded)/(parseFloat(bowlers[i].ballsBowled)/6)).toFixed(2));
    }
    return bowlers;
}

function compare( a,b){
    if(a.economy < b.economy)
    return -1;
    if(a.economy > b.economy)
    return 1;
}