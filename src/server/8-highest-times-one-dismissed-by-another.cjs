const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObj)=>{
    let wickets = []
    for(let i in jsonObj){
        if(jsonObj[i].player_dismissed != ''){
            let found = false;
            for(let j in wickets){
                if(wickets[j].bowler==jsonObj[i].bowler && wickets[j].dissmisedPlayer==jsonObj[i].player_dismissed){
                    wickets[j].times += 1;
                    found=true;
                    break;
                }
            }
            if(!found){
                wickets.push(new wicket(jsonObj[i].bowler,jsonObj[i].player_dismissed));
            }
        }
    }
    wickets.sort(compare);
    console.log(wickets);
    fs.writeFileSync('src/public/output/8-highest-times-one-dismissed-by-another.json',JSON.stringify(wickets[0])); 
})

function compare( a, b){
    if(a.times > b.times){
        return -1;
    }
    else if(a.times < b.times){
        return 1;
    }
}

function wicket(bowler,dissmisedPlayer){
    this.bowler = bowler;
    this.dissmisedPlayer = dissmisedPlayer;
    this.times = 1;
}