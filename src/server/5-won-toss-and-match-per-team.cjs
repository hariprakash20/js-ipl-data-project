const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')

csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObj)=>{
    const teams =[];
    for(let i in jsonObj){
        if(jsonObj[i].toss_winner == jsonObj[i].winner){
            teams.push(jsonObj[i].winner);
        }
    }
    let tossAndWinPerTeam = {};
    teams.forEach((team) => { tossAndWinPerTeam[team] = (tossAndWinPerTeam[team] || 0) + 1; });
    console.log(tossAndWinPerTeam);
    fs.writeFileSync('src/public/output/5-won-toss-and-match-per-team.json',JSON.stringify(tossAndWinPerTeam)); 
})