const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const csvtojson=require('csvtojson');
const fs = require('fs');
let matchIds = [];
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObj)=>{
    for(let i in jsonObj){
        if(jsonObj[i].season == 2016){
            matchIds.push(jsonObj[i].id);
        }
    }
})
csvtojson().fromFile(csvDeliveriesFilePath).then((jsonObj) => {
    let ExtraRunsPerTeam = [];
    for(let i in jsonObj){
        if(matchIds.includes(jsonObj[i].match_id)){
            let found = false;
            for(let j in ExtraRunsPerTeam){
                if(ExtraRunsPerTeam[j].team == jsonObj[i].bowling_team){
                    ExtraRunsPerTeam[j].ExtraRuns = parseInt(ExtraRunsPerTeam[j].ExtraRuns) + parseInt(jsonObj[i].extra_runs);
                    found = true;
                }
            }
            if(!found){
                ExtraRunsPerTeam.push(new Extraruns(jsonObj[i].bowling_team,jsonObj[i].extra_runs ))
            }
        }
    }
    console.log(ExtraRunsPerTeam);
    fs.writeFileSync('src/public/output/3-extra-runs-per-team-2016.json',JSON.stringify(ExtraRunsPerTeam)); 
})

function Extraruns( team, ExtraRuns){
    this.team = team;
    this.ExtraRuns = ExtraRuns;
}

    