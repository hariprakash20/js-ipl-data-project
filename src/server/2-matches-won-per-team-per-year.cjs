const csvFilePath= 'src/data/matches.csv'
const csvtojson=require('csvtojson');
const fs = require('fs');
csvtojson()
.fromFile(csvFilePath)
.then((jsonObj)=>{
    const winners = [];
    for(let i in jsonObj){
        let winner = jsonObj[i].winner;
        let year = jsonObj[i].season;
        let found =false;
        for(let j in winners){
            if(winners[j].year==year && winners[j].team==winner){
                winners[j].wins += 1;
                found = true;
                break;
            }
        }
        if(!found){
            winners.push(new entry(jsonObj[i].season,jsonObj[i].winner));
        }
    }
    fs.writeFileSync("src/public/output/2-matches-won-per-team-per-year.json", JSON.stringify(winners));
    console.log(winners);

})

class entry {
    constructor(year, team) {
        this.year = year;
        this.team = team;
        this.wins = 1;
    }
}