const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const csvtojson=require('csvtojson')
const fs = require('fs')
let matchIds = [];
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObj)=>{
    for(let i in jsonObj){
        if(jsonObj[i].season == 2015){
            matchIds.push(jsonObj[i].id);
        }
    }
})
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObj)=>{
    let bowlers =[];
    for(let i in jsonObj){
        if(matchIds.includes(jsonObj[i].match_id)){
            let found = false;
            for(let j in bowlers){
                if(bowlers[j].name == jsonObj[i].bowler){
                    if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
                        bowlers[j].ballsBowled++;
                    }
                    bowlers[j].runsConceded = parseInt(bowlers[j].runsConceded) + parseInt(jsonObj[i].total_runs) - parseInt(jsonObj[i].legbye_runs) -parseInt(jsonObj[i].bye_runs) -parseInt(jsonObj[i].penalty_runs);
                    found = true;
                }
            }
            if(!found){    
                let runsConceded = parseInt(jsonObj[i].total_runs) - parseInt(jsonObj[i].legbye_runs) -parseInt(jsonObj[i].bye_runs) -parseInt(jsonObj[i].penalty_runs);
                let ballsBowled = 0
                if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
                    ballsBowled++;
                }
                bowlers.push(new bowlerStats(jsonObj[i].bowler, runsConceded , ballsBowled))

            }
        }
    }
    for(let i in bowlers){
        bowlers[i].economy = parseFloat(bowlers[i].runsConceded/(bowlers[i].ballsBowled/6)).toFixed(2);
    }
    bowlers.sort(compare);
    let top10Bowlers = bowlers.slice(0,10);
    console.log(top10Bowlers);



    //fs.writeFileSync('src/public/output/4-Top-10-economical-bowlers-2015.json',JSON.stringify(top10Bowlers)); 
})

function compare( a, b ) {
    if ( parseFloat(a.economy) < parseFloat(b.economy) ){
      return -1;
    }
    if ( parseFloat(a.economy) > parseFloat(b.economy) ){
      return 1;
    }
    return 0;
  }
  

function bowlerStats(name, runsConceded, ballsBowled){
    this.name = name;
    this.runsConceded = runsConceded;
    this.ballsBowled = ballsBowled;
    this.economy = 0;
}