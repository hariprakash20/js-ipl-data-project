const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
let matchesPerYear = {};
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObj)=>{ 
    for(let i in jsonObj){
        let year = jsonObj[i].season;
        if(matchesPerYear[jsonObj[i].season] == undefined){
            matchesPerYear[jsonObj[i].season] = [];
        }
        matchesPerYear[year].push(jsonObj[i].id);
    }
})
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObj)=>{
    let batsmen = []; 
    for(let i in jsonObj){
        let matchId = jsonObj[i].match_id;
        let year = findYear(matchId);
        let found = false;
        for(j in batsmen){
            if(batsmen[j].year == year && batsmen[j].name == jsonObj[i].batsman){
                batsmen[j].totalRuns = parseInt(batsmen[j].totalRuns)+ parseInt(jsonObj[i].batsman_runs);
                if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
                    batsmen[j].totalBalls += 1;
                }
                found = true;
                break;
            }
        }
        if(!found){
            let ballsBowled  = 0;
            if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
                ballsBowled = 1;
            }
            batsmen.push(new batsman(year,jsonObj[i].batsman, jsonObj[i].batsman_runs, ballsBowled));
        }
    }
    calculateStrikeRate(batsmen);
    console.log(batsmen);
    fs.writeFileSync('src/public/output/7-strikerate-of-a-batsmen-per-year.json',JSON.stringify(batsmen)); 
})

function findYear(matchId){
    for(let i in matchesPerYear){
        for(let j in matchesPerYear[i]){
            if(matchId==matchesPerYear[i][j])
            return i;
        }
        
    }
}

function calculateStrikeRate(batsmen){
    for(i in batsmen){
        batsmen[i].strikeRate = ((batsmen[i].totalRuns/batsmen[i].totalBalls)*100).toFixed(2);
    }
}

function batsman(year,name, totalRuns, totalBalls){
    this.year = year;
    this.name = name;
    this.totalRuns= totalRuns;
    this.totalBalls= totalBalls;
    this.strikeRate = 0;
}