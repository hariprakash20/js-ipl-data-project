const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObj)=>{
    let players = [];
    for(let i in jsonObj){
        let found = false;
        for(let j in players){
            if(jsonObj[i].player_of_match == players[j].name && jsonObj[i].season == players[j].year){
                players[j].potm +=1
                found =true;
            }
        }
        if(!found){
            players.push(new player(jsonObj[i].season, jsonObj[i].player_of_match, 1));
        }
    }
    players.sort(compare);
    let playerOfTheYear =[];
    for(let i in players){
        let found = false;
        for(let j in playerOfTheYear){
            if(players[i].year == playerOfTheYear[j].year){
                found = true;
            }
        }
        if(!found){
            playerOfTheYear.push(players[i])
        }
    }
    console.log(playerOfTheYear);
    fs.writeFileSync('src/public/output/6-highest-player-of-the-match-per-season.json',JSON.stringify(playerOfTheYear)); 
})
function compare( a, b ) {
    if ( parseInt(a.year) < parseInt(b.year) ){
      return 1;
    }
    else if ( parseInt(a.year) > parseInt(b.year) ){
      return -1;
    }
    else if(parseInt(a.potm) < parseInt(b.potm)){
        return 1;
    }
    else if(parseInt(a.potm) > parseInt(b.potm)){
        return -1;
    }
  }

function player(year, name, potm ){
    this.year = year;
    this.name = name;
    this.potm = potm;
}